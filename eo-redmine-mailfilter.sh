#!/bin/sh

if ! echo $RECIPIENT | grep -q '^support-[a-z0-9-]*@.*$'; then
        exit 1
fi

export REDMINE_KEY=`cat /etc/redmine-key`
export REDMINE_URL='https://dev.entrouvert.org'
export ATTACHMENTS_BLACKLIST=/etc/redmine.attachments.blacklist.json

export PROJECT=`echo $RECIPIENT | sed 's/support-\([a-z0-9-]*\)@.*/\1/'`

python /usr/bin/mail2redmine.py
