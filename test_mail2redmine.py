# -*- coding: utf-8 -*-

import os
import glob
import pytest
import mock

import mail2redmine

def pytest_generate_tests(metafunc):
    if 'dump' in metafunc.fixturenames:
        metafunc.parametrize('dump', os.listdir('mail_dumps'), indirect=True)

@pytest.fixture
def dump(request):
    return open(os.path.join('mail_dumps', request.param))

def test_parsing_subjects(dump):
    mail = mail2redmine.email.message_from_file(dump)
    subject = mail2redmine.parse_header(mail['Subject'])
    assert subject
    assert isinstance(subject, str)
    assert subject in (u'Pour paiement immédiat', u'Facture Infopole à régler',
                       u'Тест на русском', u'test', u'Un ticket à tester',
                       u'[Fwd: A nouveau  du code reçu]',
                       u'[Châteauroux] Pas de visu des demandes à traiter pour un utilisateur donné')

@pytest.mark.parametrize('mail_dump', ('mail_dumps/3515ed61-bb12-4f41-ac25-81d468d2c80f.mail',
                                       'mail_dumps/711d5ffc-b153-46e1-ba21-30c6a1b45444.mail'))
def test_parsing_attachments(mail_dump):
    mail = mail2redmine.email.message_from_file(open(mail_dump))
    attachments = []
    for data in mail.walk():
        attachment = mail2redmine.parse_attachment(data)
        if attachment:
            assert 'path' in attachment
            assert 'content_type' in attachment
            assert 'filename' in attachment
            assert isinstance(attachment['filename'], str)
            attachments.append(attachment)
    assert attachments

@pytest.mark.parametrize('message', ('mail_dumps/1461591292.25165_17.dor-lomin_2.mail',))
def test_mail_with_two_attachments(message):
    mail = mail2redmine.email.message_from_file(open(message))
    attachments = []
    for data in mail.walk():
        attachment = mail2redmine.parse_attachment(data)
        if attachment:
            assert 'path' in attachment
            assert 'content_type' in attachment
            assert 'filename' in attachment
            assert isinstance(attachment['filename'], str)
            assert attachment['filename'] in (u'201601-mise-a-jour-Publik.pdf',
                                u'Capture d\'écran de 2014-12-23 17:03:40.png')
            attachments.append(attachment)
    assert len(attachments) == 2

@pytest.mark.parametrize('message', ('mail_dumps/1461591292.25165_17.dor-lomin_2.mail',))
def test_mail_attachment_blacklist(message):
    mail = mail2redmine.email.message_from_file(open(message))
    attachments = []
    blacklist = {'sha1sums': ['ab7c57983551204500d67f8ec3dd54a01e81d75d']}
    for data in mail.walk():
        attachment = mail2redmine.parse_attachment(data, blacklist=blacklist)
        if attachment:
            assert 'path' in attachment
            assert 'content_type' in attachment
            assert 'filename' in attachment
            assert isinstance(attachment['filename'], str)
            assert attachment['filename'] == u'201601-mise-a-jour-Publik.pdf'
            attachments.append(attachment)
    assert len(attachments) == 1


@mock.patch('mail2redmine.Redmine')
def test_ticket_creation_by_unknown_user(mocked_redmine, dump):
    user = mock.Mock()
    user.filter.return_value = []
    mocked_redmine.return_value = mock.Mock(user=user)
    with pytest.raises(mail2redmine.UnknownUser):
        mail2redmine.create_ticket(dump.read())


@mock.patch('mail2redmine.send_mail')
@mock.patch('mail2redmine.Redmine')
def test_ticket_creation(mocked_redmine, mocked_sendmail, dump):
    issue = mock.Mock()
    user = mock.Mock()
    user.filter.return_value = [mock.Mock(login='foo@example.com')]
    user.id.return_value = 42
    member = mock.Mock(user=user, roles=mock.Mock(get=lambda r: True))
    membership = mock.Mock(filter=lambda project_id: [member])
    issue.create.return_value = mock.Mock(id='3', url='ticket 3 url')
    mocked_redmine.return_value = mock.Mock(issue=issue, user=user,
                            project_membership=membership)
    mail2redmine.create_ticket(dump.read())
